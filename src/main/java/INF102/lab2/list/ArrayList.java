package INF102.lab2.list;

import java.util.Arrays;

public class ArrayList<T> implements List<T> {

	
	public static final int DEFAULT_CAPACITY = 10;
	
	private int n;
	
	private Object elements[];

	private int capacity;
	
	public ArrayList() {
		elements = new Object[DEFAULT_CAPACITY];
		capacity = DEFAULT_CAPACITY;
	}
		
	@Override
	public T get(int index) {
		if (index > size()-1 || index < 0) {
			throw new IndexOutOfBoundsException(index);
		}
		else {
			return (T) elements[index];
		}
		
	}
	
	@Override
	public void add(int index, T element) {
		if (size() == capacity) {
			int newCapacity = (int) (capacity + (0.5 * capacity));
			Object newElementList[] = new Object[newCapacity];
			for (int i = 0; i < size(); i++) {
				newElementList[i] = elements[i];
			}
			elements = newElementList;
			capacity = newCapacity;
		}
		if (size() == 0) {
			elements[index] = element;
			n++;
		}
		else if (index < 0 || index > capacity) {
			throw new IndexOutOfBoundsException();
		}
		else {
			int size = size();
			while (index <= size) {
				T temp = (T) elements[index];
				elements[index] = element;
				element = temp;
				index++;
			}
			n++;
		}
		
	}
	
	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n*3 + 2);
		str.append("[");
		for (int i = 0; i < n; i++) {
			str.append((T) elements[i]);
			if (i != n-1)
				str.append(", ");
		}
		str.append("]");
		return str.toString();
	}

}